# PyTransductor

A minimalistic library for doing text transformation and processing in python.

----
As an example, of it is typical capacity, we may want to process content inside HTML tags to lowercase it, 
but ignore the rest part of text between the tags and inside the strings (links, css):

```
from transductor import Transductor

transition_table = {
        'text': [('<', None, 'tag')],  # when we encounter a <, this is the beginning of a tag
        'tag': [('>', None, 'text'),  # a tag is finished by >
                ('"', None, 'string'),  # but it can contain strings that are started by "
                ('[A-Z]', lambda x: x.group(0).lower())  # I want to transform uppercase into lowercase
                ],
        'string': [
            ('"', None, 'tag'),  # string are finished by "
            (r'[\\].', None)  # if there is a backslash it will be attached to the next character
        ]
    }


tt = Transductor(transition_table, "text")
tt.process('<A href="http://g.gl/3Mg3">A LINK</A>')

#'<a href="http://g.gl/3Mg3">A LINK</a>'
```          


