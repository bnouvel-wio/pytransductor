import setuptools

try:
    # pip < v10
    from pip.req import parse_requirements
except ImportError:
    # pip >= v10
    from pip._internal.req import parse_requirements

setuptools.setup(
    version='0.1',
    author='Bertrand Nouvel',
    name='transductor',
    description='A minimalistic library for processing, parsing and transforming texts in python using transductors.',
    packages=['transductor']
)
